/** This file should be in .gitignore */

const env = process.env.MODE || 'development';

const development = {
    app: {
        port: 8000,
        secret : 'testtest'
    },
    MONGODB_URI : "mongodb://localhost:27017/firstdb"
}
const config = {
    development
};
module.exports = config[env];