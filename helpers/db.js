const mongoose = require('mongoose');
const config = require('config.js');
mongoose.connect(config.MONGODB_URI, { useCreateIndex: true, useNewUrlParser: true,useUnifiedTopology:true });
mongoose.Promise = global.Promise;  // written for mongoose < 5

//here we can import all the models in the database from their respective js files
const models = require('../models');

module.exports = {
    UserModel: models.user.UserModel
}