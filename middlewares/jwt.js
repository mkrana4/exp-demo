const jwt = require('jsonwebtoken');
const config = require('config.js');
const userService = require('../modules/user/user.service');


const Authorization = (req,res,next)=>{
    const header = req.headers['authorization'];
    if(header){
        jwt.verify(header,config.app.secret , async (err, authorizedData) => {
            if(err){
                next(err)
            } else {
                let user = await userService.service.findUserById(authorizedData.sub);
                req.user = authorizedData;
                next();
            }
        }).catch(next);
    }
    else{
        res.status(401).json({status : "error", message: 'Need token' });
    }
}

module.exports = {
    Authorization
};