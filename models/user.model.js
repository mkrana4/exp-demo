const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    email:{
        type:String,
        unique:true,
        required:true
    },
    hash:{
        type:String,
        required:true
    },
    name:{
        type:String,
        required:true
    }
}, { discriminatorKey: 'kind' })
var UserModel = mongoose.model('users', userSchema);
userSchema.set("toJSON", { virtuals: true });


module.exports = {
    UserModel
};