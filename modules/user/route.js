const express = require('express');
const router = express.Router();
const expressValidator = require('express-validator');
router.use(expressValidator());
const {Authorization} = require('middlewares/jwt');
const validator = require('./validator');
const controller = require('./user.controller');
router.get('/', (req, res) => res.send('Hello user World!'));
router.post('/login',validator.login,controller.login);
router.get('/list',Authorization,controller.users);
router.post('/create',validator.user_add,controller.create_user);
router.put('/update',Authorization,validator.user_update,controller.update_user);

module.exports = router