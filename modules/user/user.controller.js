// import {asyncMiddleware} from './admin.service'
const services = require('./user.service')
const asyncMiddleware = services.asyncMiddleware;
const service = services.service;

const create_user = asyncMiddleware( async (req,res,next)=>{
    let data = await service.createUser(req.body);
    res.status(200).json({success:true,message:"saved",data:data});
});
const login = asyncMiddleware( async (req,res,next)=>{
    console.log("login")
    let users = await service.login(req.body);
    res.status(200).json({data:users});
});
const users = asyncMiddleware( async (req,res,next)=>{
    let users = await service.userList({...req.body,...req.query});
    res.status(200).json(users);
});
const update_user = asyncMiddleware( async (req,res,next)=>{
    await service.updateUserStatus(req.body);
    res.status(200).json({success:true,"message":"updated"});
});

const delete_user = asyncMiddleware( async (req,res,next)=>{
    await service.deleteUser(req.body);
    res.status(200).json({"status":"ok","message":"removed"});
});
module.exports = {
    login
    , create_user
    , users
    , update_user
    , delete_user
};