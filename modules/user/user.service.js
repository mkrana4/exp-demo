const db = require('helpers/db.js');
const bcrpyt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config.js');

const asyncMiddleware = fn => (req,res,next)=>{
    Promise.resolve(fn(req,res,next)).catch(next);
}


const findUserById = async (id) =>{
    let user=  await db.UserModel.findOne({_id:id});
    if(user){
        return user;
    }
    else{
        throw 'user not found'
    }
}

const createUser = async (data) =>{
    let hash = bcrpyt.hashSync(data.password, 10);
    let saveData = new db.UserModel({email:data.email,name:data.name,hash: hash});
    await saveData.save()
    return saveData;
};
const login = async (reqData) =>{
    let user =  await db.UserModel.findOne({email: reqData.email});
    if(user){
        if(bcrpyt.compareSync(reqData.password, user.hash)){
            let token = jwt.sign({ sub: user._id}, config.app.secret);
            return {
                success: true,
                message: "Login",
                data:user,
                token:token
            }
        }
        else{
            throw 'Password is wrong'
        }
    }
    else{
        throw 'Email is wrong'
    }
}
const userList = async (data) =>{
    let limit = (data.per_page) ? JSON.parse(data.per_page) : 10;
    let skip = limit  * ((data.page) ? JSON.parse(data.page)-1 : 0);
    let sort_key = (data.sort_key) ? data.sort_key : 'created_at';
    let sort = (data.sort) ? data.sort : 'descending';
    let total = await db.UserModel.countDocuments().lean();
    let users = await db.UserModel.find().sort([[sort_key,sort]]).skip(skip).limit(limit).lean();
    return {data:users,sort,per_page:limit,page:data.page,total,total_pages:Math.ceil(total/limit)};
};
const updateUser =  async (data) =>{
    let saveData = await db.UserModel.updateOne({_id:mongoose.Types.ObjectId(data.user_id)},{name:data.name});
    return saveData;
};
const deleteUser =  async (data) =>{
    await db.UserModel.deleteOne({_id:mongoose.Types.ObjectId(data.user_id)});
    return true;
};


const service = {
    findUserById
    , createUser
    , login
    , userList
    , updateUser
    , deleteUser
};
module.exports = {asyncMiddleware,service}
