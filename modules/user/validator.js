const is_valid = (req,res,next) => {
    req.getValidationResult().then(function (result) {
        let errorArr = [];
        if (result.isEmpty()===false) {
            result.array().forEach(error => {
                errorArr.push(error.msg)
            });
            res.status(400).json({success:false,message:errorArr});;
        }
        else {
            next();
        }
    });
}
module.exports = {
    login : (req,res,next) => {
        req.check('email').notEmpty().withMessage('email is required')
        req.check('password').notEmpty().withMessage('password is required');
        return is_valid(req,res,next);
    },
    user_add : (req,res,next) => {
        req.check('email').notEmpty().withMessage('email is required')
        req.check('password').notEmpty().withMessage('password is required');
        req.check('name').notEmpty().withMessage('name is required');
        return is_valid(req,res,next);
    },
    user_update : (req,res,next) => {
        req.check('user_id').notEmpty().withMessage('User-id is required')
        .custom(value => mongoose.Types.ObjectId.isValid(value)).withMessage('Invalid user-id');
        req.check('name').notEmpty().withMessage('name is required');
        return is_valid(req,res,next);
    }
}