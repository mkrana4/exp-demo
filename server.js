require('rootpath')();
const config = require('./config.js')
const express = require('express')
const app = express()
const bodyParser = require('body-parser');
const cors = require('cors');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());
// app.use(expressValidator());
// console.log(config)
// app.use(function (req, res, next) {
//     console.log("UUUUUUUUUUUUUUU")
//     next()
// })
app.get('/', (req, res) => res.send('Hello World!'))

app.use('/api/user',require('modules/user/route'))
app.listen(config.app.port, () => console.log(`app listening on port ${config.app.port}!`))